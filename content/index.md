---
title: AYA Eric Dejonckheere
---

# AYA

## [aya.io/blog](http://aya.io/blog)

[Tech blog en français : developpement, astuces, apps...](http://aya.io/blog)   

# BLOGS

## [aya.io/ericd](http://aya.io/ericd)

[Microblogging, en français.](http://ericd.calepin.co)

## [ericd.re-app.net](http://ericd.re-app.net)

[Microblogging, in English.](http://ericd.re-app.net)
